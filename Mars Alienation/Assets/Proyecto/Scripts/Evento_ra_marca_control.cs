using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evento_ra_marca_control : MonoBehaviour
{

    public GameObject txt_marca;

    //Oculta la marca de RA para que esta no se renderice
    public void hideMark(){
            txt_marca.SetActive(false);
    }


    // Muestra la marca de RA
    public void showMark(){
            txt_marca.SetActive(true);
    }


}
