using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class control_robot : MonoBehaviour
{
    public float speed = 3.0F;
    public float rotateSpeed = 3.0F;

    public AudioClip roverTouched;
    public AudioSource audioSource;

    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider.name == "Pivot")
                {
                    animator.SetTrigger("isTouched");

                    audioSource.clip = roverTouched;
                    audioSource.PlayDelayed(1);
                }
            }
        }


        // salida mediante teclado
        if(Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }

    }

    private void FixedUpdate()
    {
        CharacterController controller = GetComponent<CharacterController>();

        Gamepad gamepad = Gamepad.current;
        if (gamepad == null)
        {
            return; // No gamepad connected.
        }

        Vector2 move = gamepad.leftStick.ReadValue();

        animator.SetFloat("moveY", move.y);

        if (move.y != 0 )
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }

        transform.Rotate(0, move.x * rotateSpeed, 0);

        Vector3 direccion = transform.TransformDirection(Vector3.forward);
        controller.SimpleMove(direccion * move.y * speed);
    }
}
