using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu_boton_acciones : MonoBehaviour
{

    public AudioSource audioSource;
    public UnityEngine.Video.VideoPlayer video;

    public GameObject videoButton;

    // Update is called once per frame
    void Update()
    {
        createTouchEvents();
        
    }


    void createTouchEvents(){

        // salida mediante teclado
        if(Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }

        if (Input.GetMouseButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, 300000.0f)) {                

                    Debug.Log("Clicked: "+hit.collider.name);

                    string clickedName = hit.collider.name;
                    if(clickedName == "btn_jugar"){
                        // Event locked 
                        Debug.Log("Clicked");
                    }
                    
                    if(clickedName == "btn_trailer"){
                        StartCoroutine(ChangeScene("escena_video"));
                    }

                    if(clickedName == "btn_rover"){
                        StartCoroutine(ChangeScene("escena_control"));
                    }

                    if(clickedName == "btn_ayuda"){
                        audioSource.Play();
                        // Open help panel
                        Debug.Log("Boton de ayuda");


                        //Shows help panel
                        GameObject panelAyuda = GameObject.Find("Canvas").gameObject;
                        panelAyuda = panelAyuda.transform.Find("panel_ayuda").gameObject;
                        panelAyuda.SetActive(true);

                        //Hide Menu buttons
                        foreach(GameObject menuBtn in GameObject.FindGameObjectsWithTag("Button")){
                            menuBtn.SetActive(false);
                        }

                    }
                    if(clickedName == "btn_salir"){
                        StartCoroutine(ChangeScene("exit"));
                    }


            } // End collision            
        } 

    } // End function

    IEnumerator ChangeScene(string name)
    {
        audioSource.Play();

        yield return new WaitForSeconds(1);

        if(name == "exit")
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(name);
        }

    }


}
