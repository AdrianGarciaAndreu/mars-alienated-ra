using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class evento_btn_aceptar : MonoBehaviour
{
    // Start is called before the first frame update

    public List<GameObject> menuButtons;

    public AudioSource audioSource;

    public void closeHelpPanel(){
        audioSource.Play();

        //Hide Menu button
        foreach (GameObject btn in menuButtons){
            Debug.Log("Botones encontrados");
            btn.SetActive(true);
        }


        GameObject panelAyuda = GameObject.Find("Canvas").gameObject;
        panelAyuda = panelAyuda.transform.Find("panel_ayuda").gameObject;
        panelAyuda.SetActive(false);

    }

}
