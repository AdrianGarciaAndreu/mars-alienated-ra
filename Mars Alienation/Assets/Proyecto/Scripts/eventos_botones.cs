using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class eventos_botones : MonoBehaviour
{
    public AudioSource audioSource;

    public void event_returnToMenu(){
        StartCoroutine(ChangeScene("escena_menu"));
    }

    IEnumerator ChangeScene(string name)
    {
        audioSource.Play();

        yield return new WaitForSeconds(1);

        SceneManager.LoadScene(name);
    }

}
