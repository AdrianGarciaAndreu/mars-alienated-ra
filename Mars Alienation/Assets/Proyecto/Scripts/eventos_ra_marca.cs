using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eventos_ra_marca : MonoBehaviour
{


    public GameObject txt_marca;

    //Oculta la marca de RA para que esta no se renderice
    public void hideMark(){
        if(!GameObject.Find("Canvas").transform.Find("panel_ayuda").gameObject.activeSelf){
            txt_marca.SetActive(false);
        }
    }


    // Muestra la marca de RA
    public void showMark(){
        if(!GameObject.Find("Canvas").transform.Find("panel_ayuda").gameObject.activeSelf){
            txt_marca.SetActive(true);
        }
    }

}
